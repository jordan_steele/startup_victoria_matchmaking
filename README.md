# Overview
This project contains all the components for the Startup Victoria Matchmaking project.

# Team Members
* Bruno De Assis Marques
* Qinrui Wang
* Jordan Steele
* Amy Lu
* Xiangyu Zhou
* Dongliang Liu
* Kankai Zhang
* Jiang Zhang
* Yanyi Liang
* Mengya Wang